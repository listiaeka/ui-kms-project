//
//  ViewController.swift
//  KMSproject
//
//  Created by Listia on 8/3/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.nyambungLogin), userInfo: nil, repeats: false)
    }
    
    
    func nyambungLogin() {
        self.performSegue(withIdentifier: "nyambung", sender: self)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

