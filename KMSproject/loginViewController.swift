//
//  loginViewController.swift
//  KMSproject
//
//  Created by Listia on 8/3/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit

class loginViewController: UIViewController {

    @IBAction func register(_ sender: UIButton) {
        self.performSegue(withIdentifier: "moveRegist", sender: self)
    }
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var btn_login: buttonWithCornerRadius!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btn_login.addTarget(self, action: #selector(self.login(_:)), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }

    func login(_ sender: UIButton) {
        if (emailAddress.text?.isEmpty)! {
            self.showAlert("error", message: "Email tidak boleh kosong")
            print ("email tidak boleh kosong")
            
        } else if (password.text?.isEmpty)! {
            self.showAlert("error", message: "Password tidak boleh kosong")
            print ("password tidak boleh kosong")
            
        } else {
            self.showAlert("Login Berhasil", message: "")
            print ("Login Berhasil!")
        }
        
    }
    
    func showAlert(_ title: String, message: String) {
        let alertController = UIAlertController(title : title, message: message , preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: title, style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)

    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
