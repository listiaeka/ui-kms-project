//
//  buttonWithCornerRadius.swift
//  KMSproject
//
//  Created by Listia on 8/3/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit


@IBDesignable
class buttonWithCornerRadius: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    
}
