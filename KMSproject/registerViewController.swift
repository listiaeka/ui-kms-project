//
//  registerViewController.swift
//  KMSproject
//
//  Created by Listia on 8/3/17.
//  Copyright © 2017 Bloc. All rights reserved.
//

import UIKit

class registerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.btn_regist.addTarget(self, action: #selector(self.register(_:)), for: .touchUpInside)
        
    }
    
    @IBOutlet weak var btn_regist: buttonWithCornerRadius!
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var password: UITextField!

    @IBOutlet weak var emailAddress: UITextField!
    
    @IBOutlet weak var institution: UITextField!
    
    @IBOutlet weak var fullName: UITextField!
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func register(_ sender: UIButton){
        if (fullName.text?.isEmpty)! {
            self.showAlert("error", message: "Nama tidak boleh kosong")
            print("nama tidak boleh kosong")
            
        }else if (institution.text?.isEmpty)! {
            self.showAlert("error", message: "Institusi tidak boleh kosong")
            print("institusi tidak boleh kosong")
            
        }else if (emailAddress.text?.isEmpty)! {
            self.showAlert("error", message: "Email tidak boleh kosong")
            print ("email tidak boleh kosong")
            
        }else if (password.text?.isEmpty)! {
            self.showAlert("error", message: "Password tidak boleh kosong")
            print ("password tidak boleh kosong")
            
        }else if (confirmPassword.text?.isEmpty)! {
            self.showAlert("error", message: "Konfirmasi password tidak boleh kosong")
            print ("konfirmasi password tidak boleh kosong")
        
        }else {
            self.showAlert("Register Berhasil!", message: "")
        }
    }
    
    func showAlert(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: title, style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
